#include <stdio.h>
#include "version.h"

int main(int argc, char *argv[])
{
    printf("Gitlab test application\n\r");
    printf("version %d.%d.%d", PROJECT_MAJOR_VERSION, PROJECT_MINOR_VERSION, PROJECT_BUILD_NUMBER);
    return 0;
}
